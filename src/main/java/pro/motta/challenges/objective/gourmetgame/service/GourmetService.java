package pro.motta.challenges.objective.gourmetgame.service;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import pro.motta.challenges.objective.gourmetgame.componet.GourmetComponent;

public class GourmetService {

    private static boolean progress = true;

    /**
     * Evento do Jogo
     * 
     * @return {@link ActionListener} evento.
     */
    public static ActionListener eventGame(final Map<String, String> plateMassMap, final Map<String, String> plateNoMassMap) {
        return new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                progress = true;
                boolean r1 = GourmetComponent.showQuestionYesNo("O prato que você pensou é massa?", "Confirm");
                if (r1) {
                    questionBoot(r1, plateMassMap);
                    questionsDefault(r1, plateMassMap);
                } else {
                    questionBoot(r1, plateNoMassMap);
                    questionsDefault(r1, plateNoMassMap);
                }

            }
        };
    }

    /**
     * Perguntas e respostas avançadas do boot.
     * 
     * @param r1 true se for massa.
     * @param plateMap mapa de valor e chave de massas.
     */
    private static void questionBoot(boolean r1, Map<String, String> plateMap) {
        for (Map.Entry<String, String> map : plateMap.entrySet()) {
            boolean r4 = GourmetComponent.showQuestionYesNo("O prato que você pensou é " + map.getKey() + "?", "Confirm");
            if (r4) {
                boolean r5 = GourmetComponent.showQuestionYesNo("O prato que você pensou é " + map.getValue() + "?", "Confirm");
                if (r5) {
                    alertOk();
                    progress = false;
                    break;
                }
            }
        }
    }

    /**
     * Perguntas e respostas basicas do boot.
     * 
     * @param r1 true se for massa.
     * @param plateMap mapa de valor e chave de massas.
     */
    private static void questionsDefault(boolean r1, Map<String, String> map) {
        if (progress) {
            if (r1) {
                boolean r2 = GourmetComponent.showQuestionYesNo("O prato que você pensou é Lasanha?", "Confirm");
                if (r2) {
                    alertOk();
                } else {
                    teachBoot(map, r1);
                }
            } else {
                final boolean r3 = GourmetComponent.showQuestionYesNo("O prato que você pensou é Bolo de Chocolate?", "Confirm");
                if (r3) {
                    alertOk();
                } else {
                    teachBoot(map, r1);
                }
            }
        }
    }

    /**
     * Motor que ensinar o boot.
     * 
     * @param map mapa de valor e chave.
     * @param massa true se for massa.
     */
    private static void teachBoot(Map<String, String> map, boolean massa) {
        String showQuestionPlate = GourmetComponent.showQuestion("Qual o prato que você pensou?", "Desisto");
        if (StringUtils.isNotBlank(showQuestionPlate)) {
            String plateDefault = massa ? "Lasanha" : "Bolo de Chocolate";
            String showQuestionQuality = GourmetComponent.showQuestion(showQuestionPlate + " é ________ mas " + plateDefault + " não.", "Complete");
            if (StringUtils.isNotBlank(showQuestionQuality)) {
                map.put(showQuestionQuality, showQuestionPlate);
            }
        }
    }

    /**
     * Alerta ao acertar o prato.
     */
    private static void alertOk() {
        GourmetComponent.showMessage("Acertei de novo!", "Jogo Gourmet");
        progress = false;
    }
}
