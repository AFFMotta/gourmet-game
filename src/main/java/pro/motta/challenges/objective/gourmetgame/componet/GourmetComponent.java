package pro.motta.challenges.objective.gourmetgame.componet;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

/**
 * Componentes personalizados Java Swing.
 *
 * @author Antonio.Motta - Copyright 2019 Motta Consultoria.
 */
public class GourmetComponent {

    final static String BUTTON_TEXT_YES_KEY = "OptionPane.yesButtonText";
    final static String BUTTON_TEXT_NO_KEY = "OptionPane.noButtonText";
    final static String BUTTON_MNEMONIC_YES_KEY = "OptionPane.yesButtonMnemonic";
    final static String BUTTON_MNEMONIC_NO_KEY = "OptionPane.noButtonMnemonic";
    final static String BUTTON_TEXT_YES_VALUE = "Sim";
    final static String BUTTON_TEXT_NO_VALUE = "Não";
    final static String BUTTON_MNEMONIC_YES_VALUE = "83";
    final static String BUTTON_MNEMONIC_NO_VALUE = "78";

    /**
     * Metodo de customização de criação e customização da janela (frame).
     * 
     * @param title titulo da janela.
     * @param width largura da janela.
     * @param height altura da janela.
     * 
     * @return {@link JFrame} frame customizado.
     */
    public static JFrame jframeCustom(final String title, final int width, final int height) {
        JFrame jframe = new JFrame(title);
        jframe.pack();
        jframe.setSize(width, height);
        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        return jframe;
    }

    /**
     * Cria painel no frame.
     */
    public static JPanel createPanel() {
        return new JPanel();
    }

    /**
     * Adiciona (label) texto ao painel centralizado.
     * 
     * @param description deacrição da label.
     */
    public static JLabel createLabelTextJPanelAlignCenter(String description) {
        JLabel jlabel = new JLabel(description);
        jlabel.setHorizontalAlignment(SwingConstants.CENTER);
        Border border = jlabel.getBorder();
        Border margin = new EmptyBorder(15, 30, 5, 30);
        jlabel.setBorder(new CompoundBorder(border, margin));
        return jlabel;
    }

    /**
     * Adiciona botão
     * 
     * @param description descrição/texro do botão
     * @param eventYes evento que será executado no click do botão
     */
    public static JButton createButton(String description, ActionListener event) {
        JButton jbutton = new JButton(description);
        jbutton.addActionListener(event);
        return jbutton;
    }

    /**
     * Messagem de pergunta pergunta, com resposta SIM ou NÃO.
     * 
     * @param message pergunta.
     * @param title titulo.
     * @return true quando a resposta for sim.
     */
    public static boolean showQuestionYesNo(String message, String title) {
        configYesNoJOptionalPane();
        return JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION ? true : false;
    }

    /**
     * messagem de pergunta, com campo de entrada para resposta.
     * 
     * @param message pergunta.
     * @param title titulo.
     * @return {@link String} resposta.
     */
    public static String showQuestion(String message, String title) {
        return JOptionPane.showInputDialog(null, message, title, JOptionPane.QUESTION_MESSAGE);
    }

    /**
     * Configuração de atalhos e tradução do JOptinePane.showOptionDialog YES_NO_OPTION.
     */
    private static void configYesNoJOptionalPane() {
        UIManager.put(BUTTON_TEXT_YES_KEY, BUTTON_TEXT_YES_VALUE);
        UIManager.put(BUTTON_MNEMONIC_YES_KEY, BUTTON_MNEMONIC_YES_VALUE);
        UIManager.put(BUTTON_TEXT_NO_KEY, BUTTON_TEXT_NO_VALUE);
        UIManager.put(BUTTON_MNEMONIC_NO_KEY, BUTTON_MNEMONIC_NO_VALUE);
    }

    /**
     * Alerta de messagem de informação.
     * 
     * @param message messagem.
     * @param title titulo.
     */
    public static void showMessage(String message, String title) {
        JOptionPane.showOptionDialog(null, message, title, JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
    }

    /**
     * Mostra a janela (frame) na tela.
     */
    public static void jframeShow(JFrame jframe) {
        jframe.setLocationRelativeTo(null);
        jframe.setVisible(true);
    }
}
