package pro.motta.challenges.objective.gourmetgame;

import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import pro.motta.challenges.objective.gourmetgame.componet.GourmetComponent;
import pro.motta.challenges.objective.gourmetgame.service.GourmetService;

/**
 * Classe princial de inicialização.
 *
 * @author Antonio.Motta - Copyright 2019 Motta Consultoria.
 */
public class GourmetStart {

    /**
     * janela do jogo.
     */
    private static JFrame jframeCustom;

    /**
     * Painel (componente da janela).
     */
    private static JPanel jpanelCustom;

    /**
     * Mapa de valor e chave dos pratos que são massa.
     */
    Map<String, String> plateMassMap = new HashMap<String, String>();

    /**
     * Mapa de valor e chave dos pratos que não são massas.
     */
    Map<String, String> plateNoMassMap = new HashMap<String, String>();

    public static void main(String[] args) {
        new GourmetStart().startGame();
    }

    /**
     * Método que monta o paiel e seus componetes.
     */
    private void startGame() {
        jframeCustom = GourmetComponent.jframeCustom("Jogo Gourmet", 285, 130);
        jpanelCustom = GourmetComponent.createPanel();
        final JLabel panelTextCenter = GourmetComponent.createLabelTextJPanelAlignCenter("Pense em um prato que gosta");
        jpanelCustom.add(panelTextCenter);
        final ActionListener eventGame = GourmetService.eventGame(plateMassMap, plateNoMassMap);
        final JButton okButton = GourmetComponent.createButton("OK", eventGame);
        jpanelCustom.add(okButton);
        jframeCustom.add(jpanelCustom);
        GourmetComponent.jframeShow(jframeCustom);
    }

}
